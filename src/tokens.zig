pub const Tokenizer = struct {
    const Self = @This();

    src: []const u8,
    start: Token.Index = 0,
    current: Token.Index = 0,
    done: bool = false,

    pub fn init(src: []const u8) Self {
        return .{ .src = src };
    }

    pub fn next(self: *Self) ?Token {
        if (self.done) return null;
        self.skipSpace();

        self.start = self.current;

        if (self.peek()) |ch| {
            self.advance();

            return switch (ch) {
                '+' => self.makeToken(.tk_plus),
                '-' => self.makeToken(.tk_minus),
                '*' => self.makeToken(.tk_star),
                '/' => self.makeToken(.tk_slash),
                '=' => self.matchFatArrow(),
                '_' => self.matchUnderscore(),
                ':' => self.matchConst(),
                '0'...'9' => self.matchNumber(),
                'a'...'z', 'A'...'Z' => self.matchIdentifier(),
                else => self.makeToken(.tk_err),
            };
        }

        self.done = true;
        return self.makeToken(.tk_eos);
    }

    fn matchFatArrow(self: *Self) Token {
        if (self.peek() == '>') {
            self.advance();
            return self.makeToken(.tk_arrow_fat);
        }

        return self.makeToken(.tk_equal);
    }

    fn matchUnderscore(self: *Self) Token {
        if (self.peek()) |c| switch (c) {
            '_', 'a'...'z', 'A'...'Z', '0'...'9' => return self.matchIdentifier(),
            else => {},
        };

        return self.makeToken(.tk_underscore);
    }

    fn matchIdentifier(self: *Self) Token {
        while (self.peek()) |c| switch (c) {
            '_', 'a'...'z', 'A'...'Z', '0'...'9' => self.advance(),
            else => break,
        };

        if (self.isKeyword("let")) return self.makeToken(.tk_let);
        if (self.isKeyword("in")) return self.makeToken(.tk_in);

        return self.makeToken(.tk_identifier);
    }

    fn isKeyword(self: *Self, key: []const u8) bool {
        const str = self.src[self.start..self.current];
        if (key.len != str.len) return false;
        for (str, key) |s, k| if (s != k) return false;
        return true;
    }

    fn matchNumber(self: *Self) Token {
        var found_point = false;
        while (self.peek()) |c| switch (c) {
            '0'...'9' => self.advance(),
            '.' => if (found_point) {
                found_point = true;
                self.advance();
            } else break,
            else => break,
        };

        return self.makeToken(.tk_number);
    }

    fn matchConst(self: *Self) Token {
        if (self.peek() == ':') {
            self.advance();
            return self.makeToken(.tk_const);
        }

        return self.makeToken(.tk_const);
    }

    fn skipSpace(self: *Self) void {
        while (self.peek()) |c| switch (c) {
            ' ', '\t' => self.advance(),
            else => break,
        };
    }

    fn peek(self: Self) ?u8 {
        if (self.current >= self.src.len) return null;
        return self.src[self.current];
    }

    fn advance(self: *Self) void {
        if (self.current >= self.src.len) return;
        self.current += 1;
    }

    fn makeToken(self: Self, kind: Token.Kind) Token {
        return .{
            .kind = kind,
            .start = self.start,
            .slice = self.src[self.start..self.current],
        };
    }
};

pub const Token = struct {
    pub const Index = u32;

    kind: Kind,
    start: Index,
    slice: []const u8,

    pub const Kind = enum(u8) {
        tk_const,
        tk_colon,
        tk_equal,
        tk_arrow_fat,
        tk_identifier,
        tk_underscore,
        tk_plus,
        tk_minus,
        tk_slash,
        tk_star,
        tk_number,
        tk_let,
        tk_in,
        tk_eos,
        tk_err,
    };
};
