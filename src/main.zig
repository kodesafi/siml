const std = @import("std");
const tkn = @import("tokens.zig");
const prs = @import("parser.zig");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const ally = gpa.allocator();

    // Stdout
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();

    // Input
    const src =
        \\ add1 :: x => x + 1
    ;

    try stdout.print("== input\n{s}\n\n", .{src});

    // Tokens
    try stdout.print("== tokens\n", .{});

    var tokens = tkn.Tokenizer.init(src);

    var count: usize = 0;
    while (tokens.next()) |tk| : (count += 1) {
        try stdout.print("{:0>4} {} -> `{s}` at {}\n", .{
            count,
            tk.kind,
            tk.slice,
            tk.start,
        });
    }

    // reset tokenizer.
    tokens.start = 0;
    tokens.current = 0;
    tokens.done = false;

    try stdout.print("\n", .{});

    // Ast
    try stdout.print("== Ast\n", .{});
    var ast = try prs.parse(ally, tokens);

    defer ast.deinit();

    try stdout.print("\n", .{});
    try bw.flush();
}
