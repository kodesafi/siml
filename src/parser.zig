const std = @import("std");
const tkn = @import("tokens.zig");
const mem = std.mem;

// Pratt Parser

pub fn parse(ally: mem.Allocator, tokens: tkn.Tokenizer) Error!Ast {
    var ast = Ast.init(ally);

    var scan = Scanner.init(tokens);
    try scan.advance();

    return ast;
}

// Parse Error

const Error = mem.Allocator.Error || error{InvalidToken};

fn parseError(token: tkn.Token, msg: []const u8) Error!Expr.Index {
    std.log.err("parse -> {s} [{} : `{s}`]", .{ msg, token.kind, token.slice });
    return error.InvalidToken;
}

// Scanner

const Scanner = struct {
    const Self = @This();

    lex: tkn.Tokenizer,

    // NOTE: current and next are left undefined.
    // ----> this resolves because parse first calls Scanner.advance
    curr: tkn.Token = undefined,
    next: tkn.Token = undefined,

    fn init(lex: tkn.Tokenizer) Self {
        return .{ .lex = lex };
    }

    fn consume(self: *Self, kind: tkn.Token.Kind, msg: []const u8) !void {
        if (try self.match(kind)) return;
        _ = try parseError(self.curr, msg);
    }

    fn match(self: *Self, kind: tkn.Token.Kind) !bool {
        if (!self.check(kind)) return false;
        try self.advance();
        return true;
    }

    fn check(self: *Self, kind: tkn.Token.Kind) bool {
        return self.curr.kind == kind;
    }

    fn advance(self: *Self) !void {
        self.curr = self.next;

        if (self.lex.next()) |token| {
            if (token.kind == .tk_err) _ = try parseError(token, "Found Error token");
            self.curr = token;
        }
    }
};

// Ast

pub const Ast = struct {
    const Nodes = std.MultiArrayList(Expr);

    ally: mem.Allocator,
    nodes: Nodes = .{},

    pub fn init(ally: mem.Allocator) Ast {
        return .{ .ally = ally };
    }

    pub fn deinit(self: *Ast) void {
        self.nodes.deinit(self.ally);
    }

    fn newNode(self: *Ast) !Expr.Index {
        const index = try self.nodes.addOne(self.ally);
        return @intCast(index + 1);
    }

    fn getNode(self: Ast, id: Expr.Index) Expr {
        std.debug.assert(id > 0 and id <= self.nodes.len);
        return self.nodes.get(id - 1);
    }

    fn setNode(self: *Ast, id: Expr.Index, kind: Expr.Kind, body: Expr.Body) void {
        std.debug.assert(id > 0 and id <= self.nodes.len);
        self.nodes.set(id - 1, .{
            .kind = kind,
            .body = body,
        });
    }
};

// Expression

pub const Expr = struct {
    const NULL: Index = 0;
    pub const Index = u32;

    kind: Kind,
    body: Body,

    const Body = packed struct(u64) {
        lhs: Index = NULL,
        rhs: Index = NULL,
    };

    pub const Kind = enum(u8) {
        ex_var,
        ex_abs,
        ex_app,
        ex_let,
    };
};
